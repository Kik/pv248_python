#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys
import string
from numpy import linalg
from copy import deepcopy
from collections import defaultdict, OrderedDict

def get_augmented_matrix(matrix):
    matrix_copy = deepcopy(matrix)
    augmented_matrix = []
    for i, item in enumerate(matrix_copy):
        item.append(right_side[i])
        augmented_matrix.append(item)

    return augmented_matrix


if __name__ == '__main__':
    ALL_LETTERS = string.ascii_lowercase

    matrix = []  # left side of equation
    right_side = []

    with open(sys.argv[1], mode='r', encoding='utf-8') as f:
        all_variables_with_zero = defaultdict(list)
        for line in f:
            eqn_sides = line.split(' = ')

            right = int(re.search(r'(\d+)', eqn_sides[1]).group(1))
            if '-' in eqn_sides[1]:
                right =  - right
            right_side.append(right)

            eqn_sides[0] = eqn_sides[0].replace(' ', '') # handle spaces between coef and variable
            coef_and_variables = re.findall(r'([+|-]?)\s*(\d*)([a-z])', eqn_sides[0])  # tuple (+/-, coef, variable)
            count_of_variables = {}

            for item in coef_and_variables:
                coef = item[1]
                if not item[1]:
                    coef = 1

                if item[0] == '-':
                    count_of_variables[item[2]] = - int(coef)
                else:
                    count_of_variables[item[2]] = int(coef)

            for letter in ALL_LETTERS:
                if letter in count_of_variables.keys():
                    all_variables_with_zero[letter].append(count_of_variables.get(letter))
                else:
                    all_variables_with_zero[letter].append(0)

    all_variables = {}
    for key, value_list in all_variables_with_zero.items():
        nonzero_coef = [item for item in value_list if item != 0]
        if len(nonzero_coef) > 0:  # variable is at least one equation
            all_variables[key] = value_list

    all_variables_ordered = OrderedDict(sorted(all_variables.items()))

    for i in range(len(list(all_variables_ordered.values())[0])):
        tmp_array = [value[i] for value in all_variables_ordered.values()]
        matrix.append(tmp_array)

    augmented_matrix = get_augmented_matrix(matrix)
    aug_rank = linalg.matrix_rank(augmented_matrix)
    rank = linalg.matrix_rank(matrix)

    var_count = len(all_variables)

    if aug_rank != rank:
        print('no solution')

    elif var_count == rank:
        x = list(linalg.solve(matrix, right_side))

        var_result = []
        for i, var in enumerate(all_variables_ordered.items()):
            var_result.append(var[0] + ' = ' + str(x[i]))

        print('solution: ' + ', '.join(var_result))

    else:
        print('solution space dimension: ' + str(var_count - rank))
