#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import pandas as pd
import json
from numpy import median, mean, percentile
from collections import defaultdict


def get_result_json(data, row_count=None):
    '''
    print output with stats
    '''
    output = {}
    for head, points in data.items():
        # group points per one exercise/date.. [2,3,1,8,4,3] -> [2+8, 3+4, 1+3]
        res = []
        for i in range(rows):
            summary = 0
            for j in range(int(len(points) / row_count)):
                summary += points[j * rows + i]
            res.append(summary)
        points = res

        output[head] = {
            'mean': mean(points),
            'median': median(points),
            'first': percentile(points, 25),
            'last': percentile(points, 75),
            'passed': len([p for p in points if p != 0]),
        }

    return output

def get_dict_with_points(headers_dict, data):
    points_dict = defaultdict(list)
    for head, indexes in headers_dict.items():
        if head:
            for i in indexes:
                points_dict[head].extend(data.iloc[:, i])

    return points_dict

def group_headers_and_add_indexes(headers):
    date_headers_dict = defaultdict(list)
    for i, head in enumerate(headers):
        date_headers_dict[head].append(i)

    return date_headers_dict

def dates_mode(data, row_count):
    headers = data.columns.values
    date_headers = [head.split('/')[0].strip() if head != "student" else None for head in headers]  # aby to nebolo kvoli chybajucemu studentovi posunute o jeden index

    date_headers_dict = group_headers_and_add_indexes(date_headers)
    points_dict = get_dict_with_points(date_headers_dict, data)

    print(json.dumps(get_result_json(points_dict, row_count=row_count), indent=4))

def exercises_mode(data, row_count):
    headers = data.columns.values
    date_headers = [head.split('/')[1].strip() if head != "student" else None for head in headers]  # aby to nebolo kvoli chybajucemu studentovi posunute o jeden index

    date_headers_dict = group_headers_and_add_indexes(date_headers)
    points_dict = get_dict_with_points(date_headers_dict, data)

    print (json.dumps(get_result_json(points_dict, row_count=row_count), indent=4))

def deadlines_mode(data, rows_count):
    headers = data.columns.values
    date_headers = [head.strip() if head != "student" else None for head in headers]  # aby to nebolo kvoli chybajucemu studentovi posunute o jeden index

    date_headers_dict = group_headers_and_add_indexes(date_headers)
    points_dict = get_dict_with_points(date_headers_dict, data)
    print (json.dumps(get_result_json(points_dict, row_count=rows_count), indent=4))


if __name__ == '__main__':
    file = sys.argv[1]
    mode = sys.argv[2]

    data = pd.read_csv(file)
    rows = data.shape[0]  # count of rows

    if mode == "dates":
        dates_mode(data, rows)
    elif mode == "deadlines":
        deadlines_mode(data, rows)
    elif mode == "exercises":
        exercises_mode(data, rows)
