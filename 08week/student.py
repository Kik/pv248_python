#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import pandas as pd
from numpy import median, mean, cumsum
import numpy as np
from collections import defaultdict
from datetime import datetime, timedelta


def get_points_per_exercise(data, row_count=None):
    headers = data.columns.values
    date_headers = [head.split('/')[1].strip() if head != "student" else None for head in headers]  # aby to nebolo kvoli chybajucemu studentovi posunute o jeden index

    points_dict = group_headers_and_points(date_headers, data)
    if row_count:
        points_per_exercise = []
        for points in points_dict.values():
            points_per_exercise.append(sum(points) / row_count)

    else:
        points_per_exercise = []
        for points in points_dict.values():
            points_per_exercise.append(sum(points))

    return points_per_exercise

def get_average_points(points_dict, row_count):
    aver_points_per_exercise = defaultdict(list)
    for head, points in points_dict.items():
        aver_points_per_exercise[head].append(sum(points) / row_count)

    return aver_points_per_exercise

def group_headers_and_points(date_headers, data):
    date_headers_dict = defaultdict(list)
    for i, head in enumerate(date_headers):
        date_headers_dict[head].append(i)

    points_dict = defaultdict(list)
    for head, indexes in date_headers_dict.items():
        if head:
            for i in indexes:
                points_dict[head].extend(data.iloc[:, i])

    return points_dict

def get_points_per_date(data, row_count=None):
    headers = data.columns.values
    date_headers = [head.split('/')[0].strip() if head != "student" else None for head in headers]

    points_dict = group_headers_and_points(date_headers, data)
    if row_count:
        points_dict = get_average_points(points_dict, row_count=row_count)

    return points_dict

def regresion(dates, points):
    x = np.array(dates)
    y = np.array(points)

    degrees = [1]  # list of degrees of x to use
    matrix = np.stack([x ** d for d in degrees], axis=-1)
    coeff = np.linalg.lstsq(matrix, y, rcond=-1)[0]

    # fit = np.dot(matrix, coeff)
    # print("Fitted curve/line", fit)

    return coeff[0]

def predict_date(slope):
    days_to_20_points = 20.0 / slope
    days_to_16_points = 16.0 / slope

    date16 = (datetime(2018, 9, 17) + timedelta(days=days_to_16_points)).strftime('%Y-%m-%d')
    date20 = (datetime(2018, 9, 17) + timedelta(days=days_to_20_points)).strftime('%Y-%m-%d')
    return date16, date20

def print_output(points_per_exercise, slope, date16, date20):
    output = {
        'mean': mean(points_per_exercise),
        'median': median(points_per_exercise),
        'total': sum(points_per_exercise),  # total points
        'passed': len([point for point in points_per_exercise if point != 0]),
        'regression slope': slope,
        'date 16': date16,
        'date 20': date20,
    }
    print (json.dumps(output, indent=4))

def run():
    file = sys.argv[1]
    id = sys.argv[2]

    data = pd.read_csv(file)
    rows = data.shape[0]  # count of rows  TODO handling neex. IDcka

    if id == 'average':
        points_per_exercise = get_points_per_exercise(data, row_count=rows)
        points_per_date = get_points_per_date(data, row_count=rows)

    else:
        id_data = data.loc[data['student'] == int(id)]
        if id_data.empty:  # if student with ID does not exist
            print({})
            return

        points_per_exercise = get_points_per_exercise(id_data)
        points_per_date = get_points_per_date(id_data)

    points = [(d, sum(p)) for d, p in sorted(points_per_date.items())]

    dates = [(datetime.strptime(p[0], "%Y-%m-%d") - datetime(2018, 9, 17)).days for p in points]
    cum_points = cumsum([p[1] for p in points])

    slope = regresion(dates, cum_points)
    if slope == 0:
        date16, date20 = "inf", "inf"
    else:
        date16, date20 = predict_date(slope)

    print_output(points_per_exercise, slope, date16, date20)


if __name__ == '__main__':
    run()