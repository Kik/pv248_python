#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re


class Print:
    def __init__(self, edition, print_id, partiture):
        self.edition = edition
        self.print_id = print_id
        self.partiture = partiture

    def format(self):
        print('Print Number: ' + str(self.print_id))

        composition = self.edition.composition
        if composition.authors:
            all_composers = []
            for comp in self.edition.composition.authors:
                if comp.born or comp.died:
                    all_composers.append(
                        comp.name + '(' + self.handle_none(comp.born) + '--' + self.handle_none(comp.died) + ')')
                else:
                    all_composers.append(comp.name)

            print('Composer: ' + ';'.join(all_composers))

        if composition.name:
            print('Title: ' + composition.name)

        if composition.genre:
            print('Genre: ' + composition.genre)

        if composition.key:
            print('Key: ' + composition.key)

        if composition.year:
            print('Composition Year: ' + composition.year)

        if self.edition.name:
            print('Edition: ' + self.edition.name)

        if self.edition.authors:
            print('Editor: ' + ';'.join([author.name for author in self.edition.authors if author.name != None]))

        if composition.voices:
            for i, voice in enumerate(composition.voices):
                if voice.v_range and voice.name:
                    print('Voice ' + str(i + 1) + ': ' + voice.v_range + ';' + voice.name)
                elif not (voice.v_range or voice.name):
                    print('Voice ' + str(i + 1) + ':')
                else:
                    print('Voice ' + str(i + 1) + ': ' + self.handle_none(voice.v_range) + self.handle_none(voice.name))

        if self.partiture:
            print('Partiture: yes')
        else:
            print('Partiture: no')

        if composition.incipit:
            print('Incipit: ' + composition.incipit)

        print('')

    def composition(self):
        return self.edition.composition

    def handle_none(self, string):
        if not string:
            return ''
        return string


class Edition:
    def __init__(self, composition, authors, name):
        self.composition = composition
        self.authors = authors
        self.name = name


class Composition:
    def __init__(self, name, incipit, key, genre, year, voices, authors):
        self.name = name
        self.incipit = incipit
        self.key = key
        self.genre = genre
        self.year = year
        self.voices = voices
        self.authors = authors


class Voice:
    def __init__(self, name=None, v_range=None):
        self.name = name
        self.v_range = v_range


class Person:
    def __init__(self, name='', born=None, died=None):
        self.name = name
        self.born = born
        self.died = died


def load(file):
    prints = []
    set_of_pieces = []
    piece = {}

    with open(file, mode='r', encoding='utf-8') as f:
        for line in f:
            if line.startswith('Print'):
                set_of_pieces.append(piece)
                piece = {}

            item = re.match(r'([a-zA-Z0-9 ]*):([\S|\s]*)', line)
            if item:
                key, value = item.groups()
                s_value = value.strip(' \n')
                piece[key] = s_value

        set_of_pieces.append(piece)

    for piece in set_of_pieces:
        if piece:
            composers = parse_composers(piece.get('Composer'))

            voice_keys = [(voice, piece.get(voice)) for voice in piece.keys() if voice.startswith('Voice')]

            #voice_keys = [piece.get(voice) for voice in piece.keys() if voice.startswith('Voice')]
            voices = parse_voices(voice_keys)

            comp_year = parse_year(piece.get('Composition Year'))

            composition = parse_composition(
                piece.get('Title'),
                piece.get('Incipit'),
                piece.get('Key'),
                piece.get('Genre'),
                comp_year,
                voices,
                composers,
            )

            editors = parse_editors(piece.get('Editor'))
            edition = parse_edition(piece.get('Edition'), authors=editors, composition=composition)

            partiture = parse_partiture(piece.get('Partiture'))
            num_id = piece.get('Print Number')
            prints.append(parse_print(partiture, int(num_id), edition))

    prints.sort(key=lambda x: x.print_id)
    return prints


def parse_year(comp_year):
    try:
        if comp_year and int(comp_year):
            return comp_year
        else:
            return None
    except ValueError:
        if comp_year:
            year = re.search(r'(\d{4})', comp_year)
            if year:
                return year.group(1)
            else:
                return None
        else:
            return None


def parse_partiture(partiture):
    if partiture and 'yes' in partiture:
        return True
    return False


def parse_print(partiture, num_id, edition):
    return Print(edition=edition, print_id=num_id, partiture=partiture)


def parse_editors(editors):
    if editors:
        names = []
        j = 0
        splitted_editors = editors.split(', ')
        if len(splitted_editors) == 1:
            names.append(splitted_editors[0])
        else:
            for i, editor in enumerate(splitted_editors):
                if i == j:
                    if len(editor.split()) > 1:
                        names.append(editor)
                        j += 1
                    else:
                        names.append(editor + ' ' + splitted_editors[j + 1])
                        j += 2
                else:
                    continue

        person_editors = []
        for name in names:
            person_editors.append(Person(name=name.split('(')[0]))

        return person_editors

    return []


def parse_composition(name, incipit, key, genre, year, voices, authors):
    return Composition(
        name=name,
        incipit=incipit,
        key=key,
        genre=genre,
        year=year,
        voices=voices,
        authors=authors,
    )


def parse_edition(name_of_edition, authors, composition):
    return Edition(name=name_of_edition, authors=authors, composition=composition)


def parse_voices(list_of_voices):
    num_voice_pairs = {}
    for key, voice in list_of_voices:
        voice_num = re.search(r'Voice (\d+)', key).group(1)
        num_voice_pairs[voice_num] =  voice

    min_num = min([int(k) for k in num_voice_pairs.keys()])
    max_num = max([int(k) for k in num_voice_pairs.keys()])

    all_voices = []
    for i in range(min_num, max_num + 1):
        if num_voice_pairs.get(str(i)):
            all_voices.append(num_voice_pairs.get(str(i)))
        else:
            all_voices.append(None)

    voices = []
    for voice in all_voices:
        v_range = None
        name = None
        if voice:
            if '--' in voice:
                v_range = re.search(r'([a-zA-Z0-9]+--[a-zA-Z0-9]+)', voice).group(1)
                name = voice.replace(v_range, '').strip(', ;')
            else:
                name = voice

            if not name.strip():
                name = None

        v = Voice(name=name, v_range=v_range)
        voices.append(v)

    return voices


def parse_composers(composers):
    persons = []
    if composers:
        for comp in composers.split(';'):
            born, died = None, None
            c = comp.split('(')
            name = c[0]
            if len(c) == 2:  # date of birth / dead
                if '--' in c[1]:
                    born_or_died = c[1].strip('()').split('--')
                else:
                    born_or_died = c[1].strip('()').split('-')

                try:
                    if '+' in born_or_died[0]:
                        died = int(re.search(r'(\d{4})', born_or_died[0]).group(1))

                    elif '*' in born_or_died[0]:
                        born = int(re.search(r'(\d{4})', born_or_died[0]).group(1))

                    else:
                        born = int(born_or_died[0])
                except ValueError:
                    pass

                if len(born_or_died) == 2:
                    try:
                        if int(born_or_died[1]):
                            died = born_or_died[1]
                    except ValueError:
                        pass

            p = Person(name=name.strip(), born=born, died=died)
            persons.append(p)

    return persons
