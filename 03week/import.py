#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import sqlite3
import scorelib

def create_tables(db):
    cursor = db.cursor()
    cursor.execute("""
           CREATE TABLE person ( 
            id INTEGER PRIMARY KEY NOT NULL,
            born INTEGER,
            died INTEGER,
            name varchar NOT NULL );  
        """)

    cursor.execute("""
        CREATE TABLE score ( 
            id INTEGER PRIMARY KEY NOT NULL,
            name varchar,
            genre varchar,
            key varchar,
            incipit varchar,
            year INTEGER);
        """)

    cursor.execute("""
        CREATE TABLE voice (
            id INTEGER PRIMARY KEY NOT NULL,
            number INTEGER NOT NULL, -- which voice this is
            score INTEGER REFERENCES score( id ) NOT NULL,
            range varchar,
            name varchar );
        """)

    cursor.execute("""
        CREATE TABLE edition (
            id INTEGER PRIMARY KEY NOT NULL,
            score INTEGER REFERENCES score( id ) NOT NULL,
            name varchar,
            year INTEGER );
        """)

    cursor.execute("""
        CREATE TABLE score_author(
            id INTEGER PRIMARY KEY NOT NULL,
            score INTEGER REFERENCES score( id ) NOT NULL,
            composer INTEGER REFERENCES person( id ) NOT NULL );
        """)

    cursor.execute("""
        CREATE TABLE edition_author(
            id INTEGER PRIMARY KEY NOT NULL,
            edition INTEGER REFERENCES edition( id ) NOT NULL,
            editor INTEGER REFERENCES person( id ) NOT NULL );
        """)

    cursor.execute("""
        CREATE TABLE print ( 
            id INTEGER PRIMARY KEY NOT NULL,
             partiture char(1) default 'N' NOT NULL, -- N = No, Y = Yes, P = Partial
             edition INTEGER REFERENCES edition( id ) );
        """)

    db.commit()

def save_score_to_db(db, item):
    cursor = db.cursor()

    cursor.execute("""
        SELECT * FROM score WHERE 
        name=? 
    """, (check_None(item.name), )
    )

    for row in cursor.fetchall():
        if row[1:] == (check_None(item.name), check_None(item.genre), check_None(item.key), check_None(item.incipit), check_None(item.year)):
            cursor.execute("""
                SELECT * FROM voice WHERE score=?
            """, (row[0], ))
            agreement = 0
            for r in cursor.fetchall():
                for i, v in enumerate(item.voices):
                    if r[1] == i + 1 and r[3] == v.v_range and r[4] == v.name:
                        agreement += 1

            if agreement == len(item.voices):
                agreement = 0
                cursor.execute("""
                    SELECT * FROM score_author WHERE score=?
                """, (row[0], ))
                for author in cursor.fetchall():
                    cursor.execute("""
                        SELECT * FROM person WHERE id=?
                    """, (author[2], ))
                    for auth in cursor.fetchall():
                        for a in item.authors:
                            if auth[3] == a.name:
                                agreement += 1

                if agreement == len(item.authors):
                    # only if agreement is in all score + its voices and authors, doesn't create new row in db
                    return True, row[0]

    cursor.execute("""
        INSERT INTO score(name, genre, key, incipit, year) VALUES (?, ?, ?, ?, ?)
    """, (check_None(item.name),
          check_None(item.genre),
          check_None(item.key),
          check_None(item.incipit),
          check_None(item.year),)
    )
    db.commit()

    return False, cursor.lastrowid

def save_person_to_db(db, item):
    cursor = db.cursor()

    person_ids = []
    for author in item.authors:
        cursor.execute("""
            SELECT * FROM person WHERE name=?
        """, (author.name,))

        rows = cursor.fetchall()
        if len(rows) > 0:
            if not (rows[0][1] and rows[0][2]):
                if author.born and not rows[0][1]:
                    cursor.execute("""
                        UPDATE person SET born=? WHERE name=?
                    """, (author.born, author.name))
                if author.died and not rows[0][2]:
                    cursor.execute("""
                        UPDATE person SET died=? WHERE name=?
                    """, (author.died, author.name))
                db.commit()
                return [rows[0][0]]
            else:
                return [rows[0][0]]

        cursor.execute("""
            INSERT INTO person(name, born, died) VALUES (?, ?, ?)
        """, (
            author.name, author.born, author.died
        ))
        person_ids.append(cursor.lastrowid)
        db.commit()

    return person_ids

def save_edition_to_db(db, item, score_id):
    cursor = db.cursor()

    if not item.name:
        item.name = None

    cursor.execute("""
        SELECT * FROM edition WHERE score=?
    """, (score_id,))

    rows = cursor.fetchall()
    for r in rows:
        if r[2] == item.name:
            return rows[0][0]

    cursor.execute("""
        INSERT INTO edition(score, name, year) VALUES (?, ?, ?)
    """, (score_id, item.name, None))
    db.commit()

    return cursor.lastrowid

def save_voice_to_db(db, item, score_id):
    cursor = db.cursor()

    for i, voice in enumerate(item.voices):
        if voice:
            if not voice.name:
                voice.name = None
            if not voice.v_range:
                voice.v_range = None

            cursor.execute("""
                INSERT INTO voice(number, score, range, name) VALUES (?, ?, ?, ?)
            """, (i + 1, score_id, voice.v_range, voice.name))

    db.commit()

def save_score_author_to_db(db, score_id, person_ids):
    cursor = db.cursor()

    for id in person_ids:
        cursor.execute("""
            INSERT INTO score_author(score, composer) VALUES (?, ?)
        """, (score_id, id))

    db.commit()

def save_edition_author_to_db(db, edition_id, person_ids):
    cursor = db.cursor()

    for id in person_ids:
        cursor.execute("""
                INSERT INTO edition_author(edition, editor) VALUES (?, ?)
            """, (edition_id, id))

    db.commit()

def save_print_to_db(db, item, edition_id):
    cursor = db.cursor()

    if item.partiture:
        partiture = 'Y'
    else:
        partiture = 'N'

    cursor.execute("""
        INSERT INTO print(id, partiture, edition) VALUES (?, ?, ?)
    """, (item.print_id, partiture, edition_id))

    db.commit()

def check_None(item):
    if not item:
        item = None

    return item

if __name__ == '__main__':
    input_file = sys.argv[1]
    output_file = sys.argv[2]

    db = sqlite3.connect('./' + output_file)
    create_tables(db)

    for item in scorelib.load(input_file):
        voice_and_author_agree, score_id = save_score_to_db(db, item.edition.composition)
        score_author_ids = save_person_to_db(db, item.edition.composition)

        edition_author_ids = save_person_to_db(db, item.edition)
        edition_id = save_edition_to_db(db, item.edition, score_id)

        if not voice_and_author_agree:
            save_voice_to_db(db, item.edition.composition, score_id)
            save_score_author_to_db(db, score_id, score_author_ids)

        save_edition_author_to_db(db, edition_id, edition_author_ids)
        save_print_to_db(db, item, edition_id)

    db.close()
