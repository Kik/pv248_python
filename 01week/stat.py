#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import sys
from collections import Counter, defaultdict

def get_composers(data):
    composers = []
    for composer in data['Composer']:
        comps = re.sub(r'\(\S+\)', '', composer).split(';')
        composers.extend([c.strip(' ,\n') for c in comps])

    for key, value in Counter([c for c in composers if c]).items():
        print('- ' + key + ': ' + str(value))

def get_centuries(data):
    years = []
    centuries = []
    for year in data['Composition Year']:
        if year:
            res = re.search(r'\d+. \d+. (\d+)', year)
            if res:
                years.append(res.group(1))
            elif 'century' in year:
                centuries.append(int(re.search(r'(\d{1,2})', year).group(1)))
            else:
                years.append(re.search(r'(\d{1,4})', year).group(1))

    centuries.extend(year_to_century(int(year)) for year in years)
    for key, value in Counter(centuries).items():
        print('- ' + str(key) + 'th century: ' + str(value))

def parse_data(file):
    data = defaultdict(list)
    with open(file, mode='r', encoding='utf-8') as f:
        for line in f:
            item = re.match(r'([a-zA-Z0-9 ]*):([\S|\s]*)', line)
            if item:
                key, value = item.groups()
                if value:
                    data[key].append(value.strip())
    return data

def year_to_century(year):
    reminder = 0
    if year % 100 > 0:
        reminder = 1

    return year // 100 + reminder


if __name__ == '__main__':
    if len(sys.argv) > 1:
        data = parse_data(sys.argv[1])
        if sys.argv[2] == 'composer':
            get_composers(data)
        elif sys.argv[2] == 'century':
            get_centuries(data)
