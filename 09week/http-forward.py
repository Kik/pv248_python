#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
from urllib import request, parse
from urllib.error import HTTPError
from socket import timeout
from http.server import BaseHTTPRequestHandler, HTTPServer

LOCAL_HOST = '127.0.0.1'
INVALID_ERROR = 'invalid json'
TIMEOUT = 'timeout'

class MyHTTPRequestHandler(BaseHTTPRequestHandler):
    def set_code_and_header(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def format_response(self, response):
        self.set_code_and_header()
        encoding = response.info().get_content_charset('utf-8')
        data = response.read().decode(encoding)

        client_response = {}
        client_response['code'] = response.getcode()
        client_response['headers'] = {head: value for head, value in response.info().items()}  # headers

        try:
            client_response['json'] = json.loads(data)
        except json.JSONDecodeError:
            client_response['content'] = data

        return client_response

    def do_GET_request(self, url, header_items, timeout_expiration=1):
        if header_items:
            headers = {name: value for name, value in header_items.items()}
        else:
            headers = {}

        req = request.Request(url, headers=headers)
        try:
            response = request.urlopen(req, timeout=timeout_expiration)
        except timeout:
            return

        return response

    def do_GET(self):
        response = self.do_GET_request(url, self.headers)
        if not response:  # timeout case
            self.write_timeout()
            return

        client_response = self.format_response(response)
        self.wfile.write(json.dumps(client_response).encode('utf-8'))
        return

    def write_invalid_json(self):
        self.set_code_and_header()
        self.wfile.write(json.dumps({"code": INVALID_ERROR}).encode('utf-8'))

    def write_timeout(self):
        self.set_code_and_header()
        self.wfile.write(json.dumps({"code": TIMEOUT}).encode('utf-8'))

    def do_POST(self):
        content_len = int(self.headers.get('content-length', 0))
        post_body = self.rfile.read(content_len)

        try:
            data = json.loads(post_body)
        except json.JSONDecodeError:
            self.write_invalid_json()
            return

        url = data.get('url')
        if not url:
            self.write_invalid_json()
            return

        method = data.get('type')
        headers = data.get('headers')
        if not headers:
            headers = {}

        content_data = data.get('content')

        is_content = False
        if b'content' in post_body:
            is_content = True
            if not content_data:
                content = ''
            else:
                try:
                    content = json.loads(content_data)  # content is string -> must be json if it is json-like string
                except Exception:
                    content = content_data

        timeout_exp = data.get('timeout')

        if method == 'POST' and not is_content:
            self.write_invalid_json()
            return

        if not headers.get('user-agent'):
            # request library set weird user-agent -> 403 Forbidden in response
            headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'

        if not method or method == 'GET':  # GET is default method
            response = self.do_GET_request(url, headers, timeout_exp)
            if not response:  # timeout case
                self.write_timeout()
                return

            client_response = self.format_response(response)
            self.wfile.write(json.dumps(client_response).encode('utf-8'))
            return

        elif method == 'POST':
            if headers:
                post_headers = {name: value for name, value in headers.items()}
            else:
                post_headers = {}

            req = request.Request(url, data=json.dumps(content).encode('utf-8'), headers=post_headers)
            try:
                response = request.urlopen(req, timeout=timeout_exp)
            except timeout:
                self.write_timeout()
                return

            except HTTPError:
                self.set_code_and_header()
                self.wfile.write(json.dumps({'code': 404}).encode('utf-8'))
                return

            client_response = self.format_response(response)
            self.wfile.write(json.dumps(client_response).encode('utf-8'))
            return
        return

def run_server(port):
    server_address = (LOCAL_HOST, port)
    httpd = HTTPServer(server_address, MyHTTPRequestHandler)
    # print('Server is running...')
    httpd.serve_forever()

def handle_url(url):
    if 'http' in url:
        return url

    return 'http://' + url


if __name__ == '__main__':
    port = sys.argv[1]
    url = handle_url(sys.argv[2])

    run_server(int(port))