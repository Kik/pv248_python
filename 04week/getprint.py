#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import sqlite3


if __name__ == '__main__':
    print_id = sys.argv[1]
    db = sqlite3.connect('./scorelib.dat')

    cursor = db.cursor()

    # print_id -> info about author of score
    cursor.execute("""
          SELECT person.name, person.born, person.died
          FROM print INNER JOIN edition ON print.edition = edition.id 
          INNER JOIN score_author ON edition.score = score_author.score 
          INNER JOIN person ON score_author.composer = person.id
          WHERE print.id = ?
        """, (print_id, ))

    authors = []
    for row in cursor.fetchall():
        author = {
            'name': row[0],
        }
        if row[1]:
            author.update({
                'born': row[1],
            })
        if row[2]:
            author.update({
                'died': row[2],
            })

        authors.append(author)

    print (json.dumps(authors, indent=4, ensure_ascii=False))
    db.close()
