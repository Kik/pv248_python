#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import sqlite3
from collections import defaultdict

def add_to_dict_if_not_none(dict, key, value):
    if value:
        dict.update({
            key: value,
        })
    return dict

if __name__ == '__main__':
    searched_name = sys.argv[1]
    db = sqlite3.connect('./scorelib.dat')

    cursor = db.cursor()
    cursor.execute("""
      SELECT person.*, score_author.*, score.*, edition.id, edition.name
      FROM person INNER JOIN score_author ON person.id = score_author.composer
      INNER JOIN score ON score.id = score_author.score
      INNER JOIN edition ON edition.score = score.id
      WHERE person.name LIKE ?
    """, ('%' + searched_name + '%', ))

    all_info = defaultdict(list)
    # person.id | person.born | person.died | person.name | s_a.id | s_a.score | s_a.composer | score.id |
    # score.name | score.genre | score.key | score.incipit | score.year | edition.id | edition.name
    results = cursor.fetchall()
    for row in results:
        info = {
            'Print Number': '',
            'Composer': {},
            'Title': row[8],  # 8 = score.name
        }

        info = add_to_dict_if_not_none(info, 'Genre', row[9])  # 9 = score.genre
        info = add_to_dict_if_not_none(info, 'Key', row[10])  # 10 = score.key
        info = add_to_dict_if_not_none(info, 'Composition Year', row[12])  # 12 = score.year
        info = add_to_dict_if_not_none(info, 'Edition', row[14])  # 14 = edition.name

        edition_id = row[13]  # 13 = edition.id
        score_id = row[7]  # 7 = score.id
        cursor.execute("""
          SELECT score_author.composer
          FROM score_author 
          WHERE score_author.score = ?
        """, (score_id, ))

        authors = []
        for author in [r[0] for r in cursor.fetchall()]:
            cursor.execute("""
              SELECT person.name, person.born, person.died
              FROM person 
              WHERE person.id = ?
            """, (author, ))
            for author_info in cursor.fetchall():
                authors.append({
                    'name': author_info[0],
                })
                authors[-1] = add_to_dict_if_not_none(authors[-1], 'born', author_info[1])
                authors[-1] = add_to_dict_if_not_none(authors[-1], 'died', author_info[2])

        info = add_to_dict_if_not_none(info, 'Composer', authors)

        cursor.execute("""
          SELECT edition_author.editor
          FROM edition_author
          WHERE edition_author.edition = ?
        """, (edition_id, ))

        e_authors = []
        for e_author in [r[0] for r in cursor.fetchall()]:
            cursor.execute("""
              SELECT person.name, person.born, person.died
              FROM person 
              WHERE person.id = ?
            """, (e_author,))
            for e_author_info in cursor.fetchall():
                e_authors.append({
                    'name': e_author_info[0],
                })
                e_authors[-1] = add_to_dict_if_not_none(e_authors[-1], 'born', e_author_info[1])
                e_authors[-1] = add_to_dict_if_not_none(e_authors[-1], 'died', e_author_info[2])

        if e_authors:
            info.update({
                'Editor': e_authors,
            })

        info.update({
            'Voices': [],
            'Partiture': '',
        })
        info = add_to_dict_if_not_none(info, 'Incipit', row[11])  # 11 = score.incipit

        cursor.execute("""
          SELECT voice.number, voice.name, voice.range
          FROM voice
          WHERE voice.score = ? 
          ORDER BY voice.number
        """, (score_id, ))

        voices = []
        for voice in cursor.fetchall():
            voices.append({})
            voices[-1] = add_to_dict_if_not_none(voices[-1], 'name', voice[1])
            voices[-1] = add_to_dict_if_not_none(voices[-1], 'range', voice[2])

        info['Voices'].extend(voices)

        cursor.execute("""
          SELECT print.*
          FROM print
          WHERE print.edition = ?
        """, (edition_id, ))

        print_info = cursor.fetchall()
        if len(print_info) > 1:
            print('Length of print info is more than 1 - ' + print_info[0][0])

        partiture = print_info[0][1]
        if partiture == 'Y':
            partiture = True
        else:
            partiture = False

        info.update({
            'Print Number': print_info[0][0],
            'Partiture': partiture,
        })

        all_info[row[3]].append(info)

    print(json.dumps(all_info, indent=4, ensure_ascii=False))
    db.close()
