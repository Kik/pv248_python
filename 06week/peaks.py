#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import numpy as np
from numpy import fft
import wave
import struct
from operator import itemgetter


def parse_frame(channels, frame):
    data = struct.unpack('{n}h'.format(n=channels), frame)
    return float((data[0] + data[1]) / 2) if channels == 2 else float(data[0])


def read_wav(file):
    with wave.open(file, 'rb') as wav_file:
        frames = wav_file.getnframes()
        rate = wav_file.getframerate()
        channels = wav_file.getnchannels()

        data = []
        for i in range(frames):
            frame = wav_file.readframes(1)
            p = parse_frame(channels, frame)
            data.append(p)

    length_of_chunks = frames // rate

    all_peaks = []
    for i in range(length_of_chunks):
        data_fft = fft.rfft(data[i * rate: (i + 1) * rate])
        peaks = find_peaks_in_win(np.abs(data_fft), i * rate)
        all_peaks.extend(peaks)

    return all_peaks


def find_peaks_in_win(win, shift):
    all_peaks = []

    for i, value in enumerate(win):
        if value >= 20 * np.mean(win):
            all_peaks.append((i, value))

    return all_peaks


def min_and_max_peak(all_peaks):
    if not all_peaks:
        print('no peaks')
        return

    max_peak = max(all_peaks, key=itemgetter(0))
    min_peak = min(all_peaks, key=itemgetter(0))
    print('low = {minimum}, high = {maximum}'.format(minimum=min_peak[0], maximum=max_peak[0]))


if __name__ == '__main__':
    all_peaks = read_wav(sys.argv[1])
    min_and_max_peak(all_peaks)
